﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputKeys {
    public const string leftKey = "a";
    public const string rightKey = "d";
    public const string jumpKey = "w";
    public const string attackKey = "j";
    public const string actionKey = "n";
}
