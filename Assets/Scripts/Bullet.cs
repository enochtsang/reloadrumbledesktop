﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public int damage = 1;
    public float knockback_factor = 10f;
    private Vector3 knockback;
    private bool hasCollided;

    void Start () {
        Destroy(gameObject, 3.0f);
        knockback = gameObject.GetComponent<Rigidbody2D>().velocity.normalized * knockback_factor;
        hasCollided = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasCollided) {
            return;
        } else  {
            hasCollided = true;
            Destroy(gameObject);
        }

        other.GetComponent<Damageable>().TakeDamage(damage, knockback);
    }
}
