﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageableSection : Damageable {
    public float damageMultiplier = 1;
    public float knockbackMultiplier = 1;

    public override void TakeDamage(int amount, Vector3 knockback) {
        gameObject.transform.parent.gameObject.GetComponent<Damageable>()
            .TakeDamage(
                (int)(amount * damageMultiplier),
                knockback * knockbackMultiplier
            );
    }
}
