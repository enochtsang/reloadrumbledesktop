﻿public static class RRServerMsgKeys {
    public const string type = "type"; // string
    public const string succeeded = "succeeded"; // bool
    public const string amount = "amount"; // int
    public const string sync = "sync"; // int
    public const string refresh = "refresh"; // bool
    public const string name = "name"; // string
}