﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using UnityEngine;

using WebSocketSharp;
using Newtonsoft.Json;

public sealed class RRServer : MonoBehaviour {
    public static RRServer instance = null;
    public delegate void Reaction(Dictionary<string, string> data);

    private const string gamePort = "29381";

    private WebSocket ws;
    private Dictionary<string, List<Reaction>> listeners;
    private Queue<Dictionary<string, string>> fromServerQueue;

    public string url { get; set; }

    void Awake() {
        if(instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        fromServerQueue = new Queue<Dictionary<string, string>>();
        listeners = new Dictionary<string, List<Reaction>>();
        foreach(var msgType in (RRServerMsgType[]) Enum.GetValues(typeof(RRServerMsgType))) {
            listeners[msgType.ToString()] = new List<Reaction>();
        }
    }

    void Update() {
        // Handle messages from server
        if(fromServerQueue == null) {
            fromServerQueue = new Queue<Dictionary<string, string>>();
        }
        while(fromServerQueue.Count > 0){
            var data = fromServerQueue.Dequeue();
            // Use a clone of list of reactions to tolerate reactions removing themselves
            if(data != null) {
                foreach(var reaction in (new List<Reaction>(listeners[data[RRServerMsgKeys.type]]))) {
                   reaction(data);
                }
            }
        }
    }

    public void Connect(string url) {
        this.url = url;
        ws = new WebSocket("ws://"+url+":"+gamePort+"/ws");
        ws.OnError += (sender, e) => {
            Debug.Log("Error: " + e.Message);
        };
        ws.OnMessage += (sender, e) => {
            try {
                fromServerQueue.Enqueue(
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(
                        e.Data.TrimStart('"').TrimEnd('"')));
            } catch (JsonReaderException err) {
                Debug.Log(err);
            }
        };
        ws.OnClose += (sender, e) => {
            Debug.Log("Closed: " + e.Reason);
            if(e.Reason == "An exception has occurred while connecting.") {
                fromServerQueue.Enqueue(new Dictionary<string, string>(){
                    {RRServerMsgKeys.type, RRServerMsgType.ServerConnectionStatusChange.ToString()},
                    {RRServerMsgKeys.succeeded, "false"},
                });
            }
        };
        ws.OnOpen += (sender, e) =>
        {
            Debug.Log("Opened: " + e.ToString() );
            fromServerQueue.Enqueue(new Dictionary<string, string>(){
                {RRServerMsgKeys.type, RRServerMsgType.ServerConnectionStatusChange.ToString()},
                {RRServerMsgKeys.succeeded, "true"},
            });
        };

        new Thread(new ThreadStart(delegate() {
            ws.Connect();
        })).Start();
    }

    public void Listen(RRServerMsgType msg, Reaction reaction) {
        if(!listeners[msg.ToString()].Contains(reaction)) {
            listeners[msg.ToString()].Add(reaction);
        }
    }

    public void StopListening(RRServerMsgType msg, Reaction reaction) {
        listeners[msg.ToString()].Remove(reaction);
    }

    public void Send(Dictionary<string, string> data) {
        if(ws == null) {
            // Debug.Log("Not connected to server");
            return;
        }
        ws.Send(JsonConvert.SerializeObject(data));
    }
}

