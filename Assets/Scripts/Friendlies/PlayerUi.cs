﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PlayerUi : MonoBehaviour {

    public GameObject Player;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        var health = Mathf.Max(Player.GetComponent<PlayerDamageable>().Health(), 0);
        transform.Find("Health").GetComponent<Text>().text =
            "Health: " + health.ToString();
    }
}
