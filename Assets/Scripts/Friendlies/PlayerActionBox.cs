﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionBox : MonoBehaviour {

    public GameObject player;

    void Start() {

    }

    void OnTriggerEnter2D(Collider2D other) {
        gameObject.SetActive(false);
    }

    public void Teleport(Vector3 newPosition) {
        player.transform.position = newPosition;
    }

    public void Heal(int n) {
        player.GetComponent<PlayerDamageable>().Heal(n);
    }

    public void GiveWeapon(GameObject weaponPrefab) {
        player.GetComponent<Player>().GiveWeapon(weaponPrefab);
    }

    public void GiveMovement(GameObject movementPrefab) {
        player.GetComponent<Player>().GiveMovement(movementPrefab);
    }

    public void GiveDefence(GameObject movementPrefab) {
        player.GetComponent<Player>().GiveDefence(movementPrefab);
    }
}
