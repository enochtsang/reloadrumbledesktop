﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageable : Damageable {

    private const float yKnockback = 20f;
    private const float xKnockback = 10f;
    private const float invincibilityTime = 1f;
    private readonly Color invincibilityColor = new Color(1f, 1f, 1f, 0.3f);

    private float invincibilityTimer;
    private int originalLayer;
    private Color originalColour;
    private bool invincible;

    // Use this for initialization
    void Start () {
        maxHealth = 20;
        health = 10;
        originalLayer = gameObject.layer;
        invincible = false;
        showDamageHeight = 2;
    }

    // Update is called once per frame
    void Update () {
        if(invincibilityTimer > 0) {
            invincibilityTimer -= Time.deltaTime;
        }

        if(invincible && invincibilityTimer <= 0) {
            ToggleInvincibility();
        }
    }

    public override void TakeDamage(int amount, Vector3 knockback)
    {
        if(invincible) {
            return;
        }
        ToggleInvincibility();

        health -= amount;
        if(health <= 0) {
            Destroy(gameObject);
        }

        if(knockback.x > 0) {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(xKnockback, yKnockback, 0);
        } else if (knockback.x < 0) {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(-xKnockback, yKnockback, 0);
        }

        ShowDamage(amount);
    }

    private void ToggleInvincibility() {
        if(!invincible) { // Turn on invincibility
            invincibilityTimer = invincibilityTime;
            gameObject.layer = LayerMask.NameToLayer("Invincible");
            originalColour = gameObject.GetComponent<SpriteRenderer>().color;
            gameObject.GetComponent<SpriteRenderer>().color = invincibilityColor;
            invincible = true;
        } else { // Turn off invincibility
            gameObject.layer = originalLayer;
            gameObject.GetComponent<SpriteRenderer>().color = originalColour;
            invincible = false;
        }
    }
}
