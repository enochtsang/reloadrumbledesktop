﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyDamageBox : MonoBehaviour {

    private Color fadeColor = new Color(1f, 1f, 1f, 1f);
    private float timer;
    private bool hasCollided;

    public float animationTime = 0.3f;
    public float liveTime = 0.05f;
    public delegate void ApplyDamage(Damageable other);
    public ApplyDamage customApplyDamage;

    void Start () {
        Destroy(gameObject, animationTime);
        timer = 0;
        hasCollided = false;
    }

    // Update is called once per frame
    void Update () {
        gameObject.GetComponent<SpriteRenderer>().color = fadeColor;
        fadeColor.a -=  Time.deltaTime / animationTime;
        timer += Time.deltaTime;
        if(timer > liveTime) {
            GetComponent<EdgeCollider2D>().enabled = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasCollided) {
            return;
        } else  {
            hasCollided = true;
        }
        if(other.gameObject.tag == "Enemy") {
            customApplyDamage(other.GetComponent<Damageable>());
        }
    }

}
