﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickBoots : Movement {

    private const float moveSpeed = 11f;
    private const float jumpVelocity = 37.0f;
    private const float maxStamina = 20f;
    private const float staminaPerSecond = 1f;
    private const int maxJumps = 2;
    private const float staminaPerJump = 0.5f;
    private const float startingStamina = 5;
    private const float helperUpdateInterval = 0.5f;

    private GameObject player;
    private Flippable playerFlip;
    private int jumpCount;

   [SerializeField] private float stamina;
    public float Stamina() { return stamina; }

    void Start () {
        player = transform.parent.gameObject;
        playerFlip = transform.parent.GetComponent<Flippable>();
        jumpCount = maxJumps;
        stamina = startingStamina;
        RRServer.instance.Listen(RRServerMsgType.MutateQuickBoots, HandleMutateQuickBoots);
        SetUiName("Quick Boots");
        InvokeRepeating("SyncWithMobile", 0f, helperUpdateInterval);
    }

    void OnDestroy() {
        RRServer.instance.StopListening(RRServerMsgType.MutateQuickBoots, HandleMutateQuickBoots);
    }

    void Update () {
        SetUiDetail("Stamina: " + System.Math.Max(0, stamina).ToString("0.00"));
        if(stamina > 0) {
            if(Input.GetKey(InputKeys.leftKey)) {
                if(playerFlip.flipped) {
                    player.BroadcastMessage("Flip");
                }
                player.transform.Translate(- moveSpeed * Time.deltaTime, 0, 0);
                stamina -= Time.deltaTime * staminaPerSecond;
            }
            else if (Input.GetKey(InputKeys.rightKey)) {
                if(!playerFlip.flipped) {
                    player.BroadcastMessage("Flip");
                }
                player.transform.Translate(moveSpeed * Time.deltaTime, 0, 0);
                stamina -= Time.deltaTime * staminaPerSecond;
            }
        }

        if(stamina >= staminaPerJump) {
            if (Input.GetKeyDown(InputKeys.jumpKey) && jumpCount > 0) {
                stamina -= staminaPerJump;
                var vel = player.GetComponent<Rigidbody2D>().velocity;
                vel.y = jumpVelocity;
                player.GetComponent<Rigidbody2D>().velocity = vel;
                jumpCount--;
            }
        }
    }

    public void HandleMutateQuickBoots(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.amount], out n)) {
            stamina += n * 3;
            stamina = System.Math.Min(stamina, maxStamina);
            stamina = System.Math.Max(0, stamina);
            SyncWithMobile();
        }
    }

    public void SyncWithMobile() {
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateQuickBoots.ToString() },
            { RRServerMsgKeys.sync, stamina.ToString() },
        });
    }

    override public void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Floor") {
            jumpCount = maxJumps;
        }
    }
}
