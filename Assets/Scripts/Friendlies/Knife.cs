﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : Weapon {

    public GameObject friendlyDamageBox;
    public SpriteRenderer brokenKnifeRenderer;

    private const int damage = 1;
    private const float knockbackFactor = 20f;
    private const int maxUses = 10;

    private const float range = 1f;
    private const float attackTime = 0.1f;
    private const float cooldown = 0.1f;
    private const float mobileUpdateInterval = 2f;

    private float timer;
    private bool extended;
    [SerializeField] private Flippable flip;
    [SerializeField] private int uses;

    // Use this for initialization
    void Start () {
        timer = 0;
        extended = false;
        RRServer.instance.Listen(RRServerMsgType.MutateKnife, HandleMutateKnife);
        uses = maxUses;
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateKnife.ToString() },
            { RRServerMsgKeys.sync, (uses).ToString() },
        });
        SetUiName("Knife");
        InvokeRepeating("SyncWithMobile", 0f, mobileUpdateInterval);
    }

    void OnDestroy() {
        RRServer.instance.StopListening(RRServerMsgType.MutateKnife, HandleMutateKnife);
    }

    // Update is called once per frame
    void Update () {
        SetUiDetail("Uses: " + uses.ToString());
        if(timer > 0) {
            timer -= Time.deltaTime;
        }

        if(timer <= cooldown - attackTime && extended) {
            transform.Translate(flip.factor * range, 0, 0);
            extended = false;
            return;
        }

        if(Input.GetKeyDown(InputKeys.attackKey)) {
            if(timer <= 0) {
                UseKnife();
            }
        }
    }

    private void UseKnife() {
        timer = cooldown;
        transform.Translate(flip.factor * range * -1, 0, 0);
        extended = true;
        if(uses > 0) {
            var damageBox = Instantiate(friendlyDamageBox, transform.position, transform.rotation);
            damageBox.GetComponent<FriendlyDamageBox>().customApplyDamage =
                delegate(Damageable other) {
                    var knockback = new Vector3(knockbackFactor, knockbackFactor * 0.5f, 0f);
                    if(!flip.flipped) {
                        knockback.x *= -1;
                    }
                    other.TakeDamage(damage, knockback);
                };
            if(flip.flipped) {
                damageBox.GetComponent<Flippable>().flipLocalPosition = false;
                damageBox.GetComponent<Flippable>().Flip();
            }
            uses--;
            SyncWithMobile();

            // use broken knife sprite
            if(uses <= 0) {
                GetComponent<SpriteRenderer>().enabled = false;
                brokenKnifeRenderer.enabled = true;
            }
        }
    }

    public void HandleMutateKnife(Dictionary<string, string> data) {
        bool refresh = false;
        if (bool.TryParse(data[RRServerMsgKeys.refresh], out refresh)) {
            if(refresh) {
                uses = maxUses;
                GetComponent<SpriteRenderer>().enabled = true;
                brokenKnifeRenderer.enabled = false;
                SyncWithMobile();
            }
        }
    }

    public void SyncWithMobile() {
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateKnife.ToString() },
            { RRServerMsgKeys.sync, uses.ToString() },
        });
    }
}
