﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Defence {

    public GameObject collidableArmor;

    void Start() {
        // detach so it has it's own collisions
        collidableArmor.transform.parent = null;
        collidableArmor.GetComponent<CollidableArmor>().SetArmoredObject(transform.parent.gameObject);
        collidableArmor.GetComponent<Flippable>().flipLocalPosition = false;
        SetUiName("Armor");
    }

    void Update() {
        var armorDurability = collidableArmor.GetComponent<CollidableArmor>().Health();
        SetUiDetail("Durability: " + armorDurability.ToString());
        collidableArmor.transform.position = transform.parent.transform.position;
        collidableArmor.transform.rotation = transform.parent.transform.rotation;
    }

    public void Flip() {
        collidableArmor.BroadcastMessage("Flip");
    }
}
