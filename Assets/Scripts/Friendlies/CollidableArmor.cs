﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidableArmor : Damageable {

    public GameObject armorSprite;
    private GameObject armoredObject;
    private int armoredObjectOriginalLayer;
    private const float mobileUpdateInterval = 2f;

    void Start() {
        ArmorEnabled(true);
        maxHealth = 4;
        health = maxHealth;
        showDamageHeight = 2.1f;
        RRServer.instance.Listen(RRServerMsgType.MutateArmor, HandleMutateArmor);
        InvokeRepeating("SyncWithMobile", 0f, mobileUpdateInterval);
        showDamageHeight = 2.1f;
        damageColor = new Color(0.7f, 0.7f, 0.7f, 1f);
        healColor = Color.clear;
    }

    void Update() {
        ArmorEnabled(health > 0);
    }

    public override void TakeDamage(int amount, Vector3 knockback) {
        if(health > 0) {
            health = System.Math.Max(health - amount, 0);
        }

        SyncWithMobile();
        // ignore knockback
        // gameObject.GetComponent<Rigidbody2D>().velocity = knockback;
        ShowDamage(amount);
    }

    public void SetArmoredObject(GameObject newArmoredObject) {
        armoredObject = newArmoredObject;
        armoredObjectOriginalLayer = newArmoredObject.layer;
        if(armoredObject.GetComponent<Flippable>().flipped) {
            BroadcastMessage("Flip");
        }
    }

    public void HandleMutateArmor(Dictionary<string, string> data) {
        int n = 0;
        if (data.ContainsKey(RRServerMsgKeys.amount)) {
            if(int.TryParse(data[RRServerMsgKeys.amount], out n)) {
                Heal(n);
            }
        }
    }

    public void SyncWithMobile() {
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateArmor.ToString() },
            { RRServerMsgKeys.sync, health.ToString() },
        });
    }

    private void ArmorEnabled(bool enabled) {
        GetComponent<BoxCollider2D>().enabled = enabled;
        armorSprite.SetActive(enabled);

        if(armoredObject != null) {
            if(enabled) {
                armoredObject.layer = LayerMask.NameToLayer("Invincible");
            } else {
                armoredObject.layer = armoredObjectOriginalLayer;
            }
        }
    }
}
