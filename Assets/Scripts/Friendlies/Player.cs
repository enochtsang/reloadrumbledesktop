﻿using System.Collections.Generic;

using UnityEngine;
using WebSocketSharp;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject weapon;
    [SerializeField] private GameObject movement;
    [SerializeField] private GameObject defence;
    private GameObject actionBox;
    [SerializeField] private Flippable flip;

    void Start() {
        actionBox = transform.Find("PlayerActionBox").gameObject;
        RRServer.instance.Listen(RRServerMsgType.MutateHealth, HandleMutateHealth);
        flip.flipLocalPosition = false;
    }

    void OnDestroy() {
        RRServer.instance.StopListening(RRServerMsgType.MutateHealth, HandleMutateHealth);
    }

    void Update() {
        if(Input.GetKeyDown(InputKeys.actionKey)) {
            actionBox.SetActive(true);
        }

        if(Input.GetKeyUp(InputKeys.actionKey)) {
            actionBox.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if(movement != null) {
            movement.GetComponent<Movement>().OnCollisionEnter2D(collision);
        }
    }

    public void GiveWeapon(GameObject newWeaponPrefab) {
        if(weapon != null) {
            Destroy(weapon);
        }
        weapon = Instantiate(newWeaponPrefab, transform);
        if(flip.flipped) {
            weapon.gameObject.BroadcastMessage("Flip");
        }
        var weaponName = weapon.name;
        if(weaponName.EndsWith("(Clone)")){
            weaponName = weaponName.Substring(0, weaponName.Length - "(Clone)".Length);
        }
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.NewWeapon.ToString() },
            { RRServerMsgKeys.name, weaponName },
        });
    }

    public void GiveDefence(GameObject newDefencePrefab) {
        if(defence != null) {
            Destroy(defence);
        }
        defence = Instantiate(newDefencePrefab, transform);
        if(flip.flipped) {
            defence.gameObject.BroadcastMessage("Flip");
        }
        var defenceName = defence.name;
        if(defenceName.EndsWith("(Clone)")){
            defenceName = defenceName.Substring(0, defenceName.Length - "(Clone)".Length);
        }
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.NewDefence.ToString() },
            { RRServerMsgKeys.name, defenceName },
        });
    }

    public void GiveMovement(GameObject newMovementPrefab) {
        if(movement != null) {
            Destroy(movement);
        }
        movement = Instantiate(newMovementPrefab, transform);
        var movementName = movement.name;
        if(movementName.EndsWith("(Clone)")){
            movementName = movementName.Substring(0, movementName.Length - "(Clone)".Length);
        }
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.NewMovement.ToString() },
            { RRServerMsgKeys.name, movementName },
        });
    }

    public void HandleMutateHealth(Dictionary<string, string> data) {
        int n = 0;
        if (int.TryParse(data[RRServerMsgKeys.amount], out n)) {
            gameObject.GetComponent<Damageable>().Heal(n);
        }
    }

}
