﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

abstract public class Movement : MonoBehaviour {
    abstract public void OnCollisionEnter2D(Collision2D collision);

    private GameObject movementUi;

    public void SetUiName(string movementName) {
        MovementUi().GetComponent<EquipmentUi>().itemName.text = movementName;
    }

    public void SetUiDetail(string detail) {
        MovementUi().GetComponent<EquipmentUi>().detail.text = detail;
    }

    private GameObject MovementUi() {
        if(movementUi == null) {
            movementUi = Instantiate(
            	Resources.Load<GameObject>("Prefabs/MovementUi"), transform);
        }
        return movementUi;
    }
}
