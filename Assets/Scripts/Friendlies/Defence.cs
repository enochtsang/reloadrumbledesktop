﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Defence : MonoBehaviour {

    private GameObject defenceUi;

    public void SetUiName(string defenceName) {
        DefenceUi().GetComponent<EquipmentUi>().itemName.text = defenceName;
    }

    public void SetUiDetail(string detail) {
        DefenceUi().GetComponent<EquipmentUi>().detail.text = detail;
    }

    private GameObject DefenceUi() {
        if(defenceUi == null) {
            defenceUi = Instantiate(
            	Resources.Load<GameObject>("Prefabs/DefenceUi"), transform);
        }
        return defenceUi;
    }
}
