﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Weapon {
    public GameObject friendlyDamageBox;

    private const float knockbackFactor = 20f;

    private const float attackTime = 0.22f;
    private const float cooldown = 0.22f;
    private const float rotPtX = -1.7f;
    private const float rotPtY = -1.3f;
    private const float prefabX = -1.9f;
    private const float prefabY = 0.08f;
    private const int maxSharpness = 5;
    private readonly Color brokenColor = new Color(0.660f, 0.406f, 0f, 1f);
    private Dictionary<int, int> sharpnessScale = new Dictionary<int, int>{
    //  {sharpness, damage}
        {0, 1},
        {1, 2},
        {2, 3},
        {3, 4},
        {4, 5},
        {5, 10} // must match maxSharpness!
    };
    private const float mobileUpdateInterval = 2f;

    private float timer;
    private bool swung;
    [SerializeField] private Flippable flip;
    [SerializeField] private int sharpness;
    private int prevFlipFactor;

    // Use this for initialization
    void Start () {
        timer = 0;
        swung = false;
        sharpness = maxSharpness;
        RRServer.instance.Listen(RRServerMsgType.MutateSword, HandleMutateSword);
        SetUiName("Sword");
        InvokeRepeating("SyncWithMobile", 0f, mobileUpdateInterval);
    }

    // Update is called once per frame
    void Update () {
        SetUiDetail("Sharpness: " + sharpness.ToString());
        if(timer > 0) {
            timer -= Time.deltaTime;
        }

        if(sharpness <= 0) {
            gameObject.GetComponent<SpriteRenderer>().color = brokenColor;
        } else {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }

        if(timer <= cooldown - attackTime && swung) {
            transform.Rotate(Vector3.forward * -90 * prevFlipFactor);
            transform.Translate(-rotPtX * flip.factor, -rotPtY, 0);
            GetComponent<SpriteRenderer>().flipY = false;
            GetComponent<SpriteRenderer>().flipX = flip.flipped;
            swung = false;
            return;
        }

        if(Input.GetKeyDown(InputKeys.attackKey)) {
            if(timer <= 0) {
                timer = cooldown;
                var damageBox = Instantiate(
                    friendlyDamageBox,
                    transform.position + new Vector3(prefabX * flip.factor, prefabY, 0f),
                    transform.rotation
                );
                if(flip.flipped) {
                    damageBox.GetComponent<Flippable>().flipLocalPosition = false;
                    damageBox.GetComponent<Flippable>().Flip();
                }

                // need to get value now, or it could change by the time it's used
                int damage = sharpnessScale[sharpness];
                damageBox.GetComponent<FriendlyDamageBox>().customApplyDamage =
                    delegate(Damageable other) {
                        var knockback = new Vector3(knockbackFactor, knockbackFactor, 0f);
                        if(!flip.flipped) {
                            knockback.x *= -1;
                        }
                        other.TakeDamage(damage, knockback);
                    };
                transform.Translate(rotPtX * flip.factor, rotPtY, 0);
                prevFlipFactor = flip.factor;
                transform.Rotate(Vector3.forward * 90 * prevFlipFactor);
                swung = true;
                if(sharpness > 0) {
                    sharpness--;
                    SyncWithMobile();
                }
            }
        }
    }

    // Intercept the BroadcastMessage meant for Flippable
    public void Flip() {
        if(swung) {
            if(flip.flipped) {
                if(prevFlipFactor != flip.factor) {
                    GetComponent<SpriteRenderer>().flipX = true;
                    GetComponent<SpriteRenderer>().flipY = true;
                } else {
                    GetComponent<SpriteRenderer>().flipX = false;
                    GetComponent<SpriteRenderer>().flipY = false;
                }
            } else {
                if(prevFlipFactor != flip.factor && swung) {
                    GetComponent<SpriteRenderer>().flipX = false;
                    GetComponent<SpriteRenderer>().flipY = true;
                } else {
                    GetComponent<SpriteRenderer>().flipX = true;
                    GetComponent<SpriteRenderer>().flipY = false;
                }
            }
        }
    }

    public void HandleMutateSword(Dictionary<string, string> data) {
        int n = 0;
        if (data.ContainsKey(RRServerMsgKeys.amount)) {
            if(int.TryParse(data[RRServerMsgKeys.amount], out n)) {
                sharpness = System.Math.Min(sharpness + n, maxSharpness);
                SyncWithMobile();
            }
        } else if(data.ContainsKey(RRServerMsgKeys.sync)) {
            if (int.TryParse(data[RRServerMsgKeys.sync], out n)) {
                sharpness = n;
            }
        }
    }

    public void SyncWithMobile() {
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateSword.ToString() },
            { RRServerMsgKeys.sync, sharpness.ToString() },
        });
    }
}
