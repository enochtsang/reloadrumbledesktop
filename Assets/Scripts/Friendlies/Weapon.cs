﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Weapon : MonoBehaviour {

    private GameObject weaponUi;

    public void SetUiName(string weaponName) {
        WeaponUi().GetComponent<EquipmentUi>().itemName.text = weaponName;
    }

    public void SetUiDetail(string detail) {
        WeaponUi().GetComponent<EquipmentUi>().detail.text = detail;
    }

    private GameObject WeaponUi() {
        if(weaponUi == null) {
            weaponUi = Instantiate(
                Resources.Load<GameObject>("Prefabs/WeaponUi"), transform);
        }
        return weaponUi;
    }
}
