﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltActionRifle : Weapon {

    public Transform bulletSpawn;

    private const int maxAmmo = 5;
    private const float cooldown = 1f;
    private const float kickback = 10f;
    protected float bulletSpeed = 70f;

    private float lastShotTime = 0f;
    public GameObject bulletPrefab;
    [SerializeField] private int ammo;
    [SerializeField] private Flippable flip;
    private const float mobileUpdateInterval = 2f;

    // Use this for initialization
    void Start () {
        RRServer.instance.Listen(RRServerMsgType.MutateBoltActionRifle, HandleMutateBoltActionRifle);
        SetUiName("Bolt Action Rifle");
        ammo = 0;
        InvokeRepeating("SyncWithMobile", 0f, mobileUpdateInterval);
    }

    // Update is called once per frame
    void Update () {
        SetUiDetail("Ammo: " + ammo.ToString());

        if(Input.GetKeyDown(InputKeys.attackKey)) {
            if(Time.time - lastShotTime > cooldown && ammo > 0) {
                Fire();
            }
        }
    }

    public void Fire() {
        lastShotTime = Time.time;
        var bullet = Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            Quaternion.identity
        );
        bullet.GetComponent<Rigidbody2D>().velocity = transform.right * -bulletSpeed * flip.factor;
        transform.parent.gameObject.GetComponent<Rigidbody2D>().velocity
            = transform.right * kickback * flip.factor;
        if(flip.flipped) {
            bullet.GetComponent<Flippable>().flipLocalPosition = false;
            bullet.GetComponent<Flippable>().Flip();
        }

        ammo--;
        SyncWithMobile();
    }

    public void HandleMutateBoltActionRifle(Dictionary<string, string> data) {
        int n = 0;
        if (data.ContainsKey(RRServerMsgKeys.amount)) {
            if(int.TryParse(data[RRServerMsgKeys.amount], out n)) {
                ammo = System.Math.Min(ammo + n, maxAmmo);
            }
            SyncWithMobile();
        }
    }

    public void SyncWithMobile() {
        RRServer.instance.Send(new Dictionary<string, string>() {
            { RRServerMsgKeys.type, RRServerMsgType.MutateBoltActionRifle.ToString() },
            { RRServerMsgKeys.sync, ammo.ToString() },
        });
    }
}
