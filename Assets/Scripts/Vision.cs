﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class Vision : MonoBehaviour {

    public List<GameObject> visibleObjects { get; private set; } = new List<GameObject>();
    public string[] tagsToSee { get; set; }

    public void OnTriggerEnter2D(Collider2D other) {
        if(!visibleObjects.Contains(other.gameObject) &&
            tagsToSee.Contains(other.gameObject.tag)) {
            visibleObjects.Add(other.gameObject);
        }
    }

    public void OnTriggerExit2D(Collider2D other) {
        if(visibleObjects.Contains(other.gameObject)) {
            visibleObjects.Remove(other.gameObject);
        }
    }
}
