﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSceneController : MonoBehaviour {

	public KillPrize killPrize;
	public GameObject[] hitList;
	public GameObject prize;

	// Use this for initialization
	void Start () {
		var lst = new List<GameObject>();
		lst.AddRange(hitList);
		// killPrize.Init(lst, Instantiate(prize));
		killPrize.InitRandomItem(lst);
	}

	// Update is called once per frame
	void Update () {

	}
}
