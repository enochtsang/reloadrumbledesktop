﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Generates metadata for a sector
public class SectorGenerator : MonoBehaviour {
    private const int sectorWidth = 8;
    private const int sectorHeight = 10;
    private const float roomXSpacing = 70;
    private const float roomYSpacing = 35;

    // The probability of between 0 and 1 of a path
    // being made at a door
    private const float pathGenerationChance = 0.6f;

    public static GameObject[,] GenerateSector(GameObject sectorRoomPrefab) {
        var sector = new GameObject[sectorHeight, sectorWidth];

        // Create entrance room
        var col = Random.Range(0, sectorWidth);
        var newRoom = Instantiate(
            sectorRoomPrefab,
            new Vector3(col * roomXSpacing, 0, 0),
            Quaternion.identity
        );
        sector[0, col] = newRoom;
        newRoom.GetComponent<SectorRoom>().AddEntrance();
        newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NW);
        newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NE);
        newRoom.GetComponent<SectorRoom>().SetFloor(0);

        // Generate paths and new room
        // except top floor
        for(int floor = 0; floor < sectorHeight-1; floor++) {
            GenerateFloor(sectorRoomPrefab, sector, floor);
        }

        // Set exit room
        return sector;
    }

    private static void GenerateFloor(
        GameObject sectorRoomPrefab,
        GameObject[,] sector,
        int floor
    ) {
        // iterate through rooms in the floor
        for(int col = 0; col < sectorWidth; col++) {
            if(sector[floor, col] == null) {
                // room does not exist
                continue;
            } else {
                GenerateNewRooms(sectorRoomPrefab, sector, floor, col);
            }
        }

        // iterate through rooms in the floor to check there is a northern path
        var pathExists = false;
        var existingRooms = new List<int>();
        for(int col = 0; col < sectorWidth; col++) {
            if(sector[floor, col] == null) {
                // room does not exist
                continue;
            } else {
                existingRooms.Add(col);
                var roomController = sector[floor, col].GetComponent<SectorRoom>();
                if(roomController.HasPath(SectorRoomDoorDirection.NW) ||
                    roomController.HasPath(SectorRoomDoorDirection.NE)) {
                    pathExists = true;
                }
            }
        }

        if(!pathExists) {
            var randomRoomCol = existingRooms[Random.Range(0, existingRooms.Count - 1)];
            // if left most room, give NE path
            // if right most room, give NW path
            // otherwise randomly pick a path
            var roomController = sector[floor, randomRoomCol].GetComponent<SectorRoom>();
            if(randomRoomCol == 0) {
                roomController.AddDoor(SectorRoomDoorDirection.NE);
            } else if (randomRoomCol == sectorWidth-1) {
                roomController.AddDoor(SectorRoomDoorDirection.NW);
            } else {
                if(Random.Range(0, 2) == 0) {
                    roomController.AddDoor(SectorRoomDoorDirection.NW);
                } else {
                    roomController.AddDoor(SectorRoomDoorDirection.NE);
                }
            }

            GenerateNewRooms(sectorRoomPrefab, sector, floor, randomRoomCol);
        }
    }

    // Create new rooms from paths on the given floor
    private static void GenerateNewRooms(
        GameObject sectorRoomPrefab,
        GameObject[,] sector,
        int floor,
        int col
    ) {
        var room = sector[floor, col].GetComponent<SectorRoom>();
        GameObject newRoom = null;

        if(room.HasPath(SectorRoomDoorDirection.NW)) {
            if(col <= 0) {
                // already left most room
                // remove this path
                room.RemoveDoor(SectorRoomDoorDirection.NW);
            } else if(sector[floor+1, col-1] != null) {
                // room already exists
                // create this path
                sector[floor+1, col-1].GetComponent<SectorRoom>()
                    .AddDoor(
                        SectorRoomDoorDirection.SE,
                        room.doors[(int)SectorRoomDoorDirection.NW]
                    );
            } else {
                // create new room
                newRoom = Instantiate(
                    sectorRoomPrefab,
                    new Vector3(
                        (col - 1) * roomXSpacing,
                        (floor + 1) * roomYSpacing,
                        0
                    ),
                    Quaternion.identity
                );
                sector[floor + 1, col - 1] = newRoom;
                newRoom.GetComponent<SectorRoom>()
                    .AddDoor(
                        SectorRoomDoorDirection.SE,
                        room.doors[(int)SectorRoomDoorDirection.NW]
                    );
                newRoom.GetComponent<SectorRoom>().SetFloor(floor+1);
                if(Random.value < pathGenerationChance) {
                    newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NW);
                }
                if(Random.value < pathGenerationChance) {
                    newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NE);
                }
            }
        }

        if(room.HasPath(SectorRoomDoorDirection.NE)) {
            if(col >= sectorWidth-1) { // already right most room
                room.RemoveDoor(SectorRoomDoorDirection.NE);
            } else if(sector[floor+1, col+1] != null) {
                // room already exists
                sector[floor+1, col+1].GetComponent<SectorRoom>()
                    .AddDoor(
                        SectorRoomDoorDirection.SW,
                        room.doors[(int)SectorRoomDoorDirection.NE]);
            } else {
                // create new room
                newRoom = Instantiate(
                    sectorRoomPrefab,
                    new Vector3(
                        (col + 1) * roomXSpacing,
                        (floor + 1) * roomYSpacing,
                        0
                    ),
                    Quaternion.identity
                );
                sector[floor + 1, col + 1] = newRoom;
                
                // set up south doors
                newRoom.GetComponent<SectorRoom>()
                    .AddDoor(
                        SectorRoomDoorDirection.SW,
                        room.doors[(int)SectorRoomDoorDirection.NE]
                    );

                // create new north paths
                newRoom.GetComponent<SectorRoom>().SetFloor(floor+1);
                if(Random.value < pathGenerationChance) {
                    newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NW);
                }
                if(Random.value < pathGenerationChance) {
                    newRoom.GetComponent<SectorRoom>().AddDoor(SectorRoomDoorDirection.NE);
                }
            }
        }
    }
}
