﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SectorRoom : MonoBehaviour {

    public GameObject entrance { get; set; }
    public GameObject[] doors { get; private set; }
    public Text floorIndicator;
    public KillPrize killPrize;
    public GameObject sectorRoomDoorPrefab;
    public GameObject sectorEntrancePrefab;

    void Awake () {
        doors = new GameObject[System.Enum.GetNames(typeof(SectorRoomDoorDirection)).Length];
    }


    public void SetFloor(int n) {
        floorIndicator.text = "F" + n.ToString();
    }

    public void AddDoor(
        SectorRoomDoorDirection direction,
        GameObject connectingDoor = null
    ) {
        if(doors[(int)direction] != null) {
            return;
        }

        var loc = Vector3.zero;
        switch(direction) {
            case SectorRoomDoorDirection.NW:
                loc = new Vector3(-24f, 7.3f, 0f);
                break;
            case SectorRoomDoorDirection.NE:
                loc = new Vector3(24f, 7.3f, 0f);
                break;
            case SectorRoomDoorDirection.SW:
                loc = new Vector3(-24f, -9.9f, 0f);
                break;
            case SectorRoomDoorDirection.SE:
                loc = new Vector3(24f, -9.9f, 0f);
                break;
        }
        var newDoor = Instantiate(
            sectorRoomDoorPrefab,
            this.transform
        );
        doors[(int)direction] = newDoor;
        newDoor.transform.localPosition = loc;
        if(connectingDoor != null) {
            newDoor.GetComponent<SectorRoomDoor>().otherDoor = connectingDoor;
            connectingDoor.GetComponent<SectorRoomDoor>().otherDoor = newDoor;
        }
    }

    public void RemoveDoor(SectorRoomDoorDirection direction) {
        Destroy(doors[(int)direction]);
        doors[(int)direction] = null;
    }

    public void AddEntrance() {
        if(entrance != null) {
            return;
        }
        entrance = (GameObject)Instantiate(sectorEntrancePrefab, this.transform);
        entrance.transform.localPosition = new Vector3(0f, 2.8f, 0f);
    }

    public bool HasPath(SectorRoomDoorDirection direction) {
        return doors[(int)direction] != null;
    }
}
