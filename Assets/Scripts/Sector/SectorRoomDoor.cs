﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectorRoomDoor : MonoBehaviour {

    public GameObject otherDoor;

    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.name == "PlayerActionBox") {
            other.gameObject.GetComponent<PlayerActionBox>()
                .Teleport(otherDoor.transform.position);
            transform.parent.GetComponent<Camera>().enabled = false;
            otherDoor.transform.parent.GetComponent<Camera>().enabled = true;
        }
    }
}
