﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] spawns;

    public List<GameObject> Spawn(Dictionary<GameObject, int> enemiesToSpawn) {
    	var spawned = new List<GameObject>();
        foreach(var entry in enemiesToSpawn) {
            for(int i = 0; i < entry.Value; i++) {
                var spawnLocation = spawns[Random.Range(0, spawns.Length)].transform.position;
                spawned.Add(Instantiate(entry.Key, spawnLocation, Quaternion.identity));
            }
        }
        return spawned;
    }

}
