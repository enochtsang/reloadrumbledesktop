﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Sector1 : MonoBehaviour {

    public GameObject playerPrefab;
    public GameObject roomPrefab;
    public GameObject rifleGuardPrefab;
    public GameObject rifleReinforcementPrefab;
    public GameObject startingMovementPrefab;
    public GameObject startingWeaponPrefab;
    public Text reinforcementsFloorProgressText;
    public Text reinforcementsTimerText;

    private const float reinforcementsInterval = 40f;
    private const int numReinforcements = 3;

    private GameObject player;
    private int reinforcementsFloorProgress;
    private float reinforcementsTimer;
    private GameObject[,] sectorMap;

    // Use this for initialization
    void Start () {
        sectorMap = SectorGenerator.GenerateSector(roomPrefab);
        for(int row = 0; row < sectorMap.GetLength(0); row++) {
            for(int col = 0; col < sectorMap.GetLength(1); col++) {
                var room = sectorMap[row, col];
                if(room == null) {
                    continue;
                }
                if(row == 0) {
                    // special case for first floor
                    room.GetComponent<Camera>().enabled = true;
                    InstantiatePlayer(room.transform.position);
                    break;
                } else {
                    var spawned = room.GetComponent<Spawner>().Spawn(new Dictionary<GameObject, int>(){
                        { rifleGuardPrefab, row }
                    });
                    room.GetComponent<SectorRoom>().killPrize.InitRandomItem(spawned);
                }
            }
        }
        reinforcementsTimer = reinforcementsInterval;
        reinforcementsFloorProgress = -1;
    }

    // Update is called once per frame
    void Update () {
        if(reinforcementsFloorProgress < sectorMap.GetLength(0)) {
            reinforcementsTimer -= Time.deltaTime;
            if(reinforcementsTimer < 0) {
                reinforcementsFloorProgress++;
                SpawnReinforcements(reinforcementsFloorProgress);
                reinforcementsTimer = reinforcementsInterval;
            }

        } else {
            reinforcementsTimerText.text = (0f).ToString("0.00");
        }

        if(reinforcementsFloorProgress >= 0) {
            reinforcementsFloorProgressText.text = "Reinforcements: F" + reinforcementsFloorProgress.ToString();
        } else {
            reinforcementsFloorProgressText.text = "Reinforcements: NA";
        }
        reinforcementsTimerText.text = reinforcementsTimer.ToString("0.00");
    }

    private void SpawnReinforcements(int floor) {
        for(int col = 0; col < sectorMap.GetLength(1); col++) {
            var room = sectorMap[floor, col];
            if(room == null) {
                continue;
            } else {
                room.GetComponent<Spawner>().Spawn(new Dictionary<GameObject, int>(){
                    { rifleReinforcementPrefab, numReinforcements }
                });
            }
        }
    }

    private void InstantiatePlayer(Vector3 position) {
        player = Instantiate(
            playerPrefab,
            position,
            Quaternion.identity
        );
        Instantiate(
            startingMovementPrefab,
            position,
            Quaternion.identity
        );
        Instantiate(
            startingWeaponPrefab,
            // spawn weapon a small distance away to force movement
            position + new Vector3(5, 0, 0),
            Quaternion.identity
        );
    }
}
