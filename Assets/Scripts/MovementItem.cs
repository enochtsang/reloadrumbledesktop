﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementItem : MonoBehaviour {

    private bool hasTriggered;
    public string movement;

    void Awake() {
        hasTriggered = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasTriggered) {
            return;
        }

        if(other.gameObject.name == "PlayerActionBox") {
            other.gameObject.GetComponent<PlayerActionBox>()
                .GiveMovement(Resources.Load<GameObject>("Prefabs/" + movement));
            hasTriggered = true;
            Destroy(transform.parent.gameObject);
        }
    }
}
