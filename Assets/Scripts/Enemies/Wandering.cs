﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Flippable))]
public class Wandering : MonoBehaviour {

    public float moveSpeed = 1.0f;
    public float randomFlipChance = 0.01f;

	// Update is called once per frame
	void Update () {
        if(GetComponent<Flippable>().flipped) {
            transform.Translate(moveSpeed * Time.deltaTime, 0, 0);
        } else {
            transform.Translate(-moveSpeed * Time.deltaTime, 0, 0);
        }

        if(Random.value < randomFlipChance) {
            BroadcastMessage("Flip");
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.CompareTag("Platform Edge"))
        {
            BroadcastMessage("Flip");
        }
    }
}
