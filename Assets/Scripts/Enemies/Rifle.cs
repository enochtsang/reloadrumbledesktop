﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour {

    public float bulletSpeed = 50f;
    public float cooldown = 3f;
    public GameObject bulletPrefab;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] protected Flippable flip;

    private float cooldownTimer = 0f;

    void Awake() {
        Init();
    }

    void Update () {
        if(!CanFire()) {
            cooldownTimer -= Time.deltaTime;
        }
    }

    public void Init() {
    }

    public void Aim(Vector3 target) {
        Vector3 vectorToTarget = target - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        if(!flip.flipped) {
            angle += 180f;
        }
        Quaternion quat = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = quat;
    }

    public void ResetAim() {
        transform.rotation = Quaternion.identity;
    }

    public void Flip() {

    }

    public bool CanFire() {
        return cooldownTimer <= 0;
    }

    public GameObject Fire() {
        if(!CanFire()) {
            return null;
        }
        cooldownTimer = cooldown;
            var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            transform.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = transform.right * -bulletSpeed;
        if(flip.flipped) {
            bullet.GetComponent<Rigidbody2D>().velocity *= -1f;
        }
        return bullet;
    }
}
