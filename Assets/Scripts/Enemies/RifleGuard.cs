﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rifle))]
[RequireComponent(typeof(Vision))]
[RequireComponent(typeof(Flippable))]
[RequireComponent(typeof(Wandering))]
public class RifleGuard : MonoBehaviour {

    public Vision vision;
    public Rifle rifle;
    public Wandering wandering;
    [SerializeField] private Flippable flip;
    public GameObject healthPackPrefab;

    private GameObject target;

    void Awake() {
        vision.tagsToSee = new string[] { "Player" };
        target = null;
    }

    void Start () {
        flip.flipLocalPosition = false;
    }

    void OnDisable() {
        Instantiate(healthPackPrefab, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update () {
        if(target != null) {
            wandering.enabled = false;
            if(vision.visibleObjects.Count == 0) {
                target = null;
                rifle.ResetAim();
            } else {
                if((target.transform.position - transform.position).x < 0) {
                    if(flip.flipped) {
                        BroadcastMessage("Flip");
                    }
                } else {
                    if(!flip.flipped) {
                        BroadcastMessage("Flip");
                    }
                }
                rifle.Aim(target.transform.position);
                if(rifle.CanFire()) {
                    rifle.Fire();
                }
            }
        } else {
            wandering.enabled = true;
            // no target, look for target
            if(vision.visibleObjects.Count > 0) {
                target = vision.visibleObjects[0];
            } else {
                target = null;
            }
        }
    }
}
