﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

    private const int healAmount = 3;
    private bool hasTriggered;

    void Awake() {
        hasTriggered = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasTriggered) {
            return;
        }

        if(other.gameObject.name == "PlayerActionBox") {
            other.gameObject.GetComponent<PlayerActionBox>()
                .Heal(healAmount);
            hasTriggered = true;
            Destroy(transform.parent.gameObject);
        }
    }
}
