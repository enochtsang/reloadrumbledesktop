﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceItem : MonoBehaviour {

    private bool hasTriggered;
    public string defence;

    void Awake() {
        hasTriggered = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasTriggered) {
            return;
        }

        if(other.gameObject.name == "PlayerActionBox") {
            other.gameObject.GetComponent<PlayerActionBox>()
                .GiveDefence(Resources.Load<GameObject>("Prefabs/" + defence));
            hasTriggered = true;
            Destroy(transform.parent.gameObject);
        }
    }
}
