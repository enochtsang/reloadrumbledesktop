﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private RRServer rrserver;

    public Button connectButton;
    public Button testButton;
    public InputField urlInputField;
    public Text message;
    private string sceneToLoad;

    // Use this for initialization
    void Start () {
        rrserver = RRServer.instance;
        connectButton.onClick.AddListener(HandleConnectButtonClicked);
        testButton.onClick.AddListener(HandleTestButtonClicked);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown("return") && urlInputField.text.Trim() != "") {
            HandleConnectButtonClicked();
        }
    }

    public void HandleConnectButtonClicked() {
        rrserver.Connect(urlInputField.text.Trim());
        message.text = "Connecting...";
        rrserver.Listen(
            RRServerMsgType.ServerConnectionStatusChange,
            HandleServerConnectionStatusChange);
        sceneToLoad = "Sector1";
    }

    public void HandleTestButtonClicked() {
        rrserver.Connect(urlInputField.text.Trim());
        message.text = "Connecting...";
        rrserver.Listen(
            RRServerMsgType.ServerConnectionStatusChange,
            HandleServerConnectionStatusChange);
        sceneToLoad = "TestScene";
    }

    public void HandleServerConnectionStatusChange(Dictionary<string, string> data) {
        bool connectionSuccessful = false;
        if (bool.TryParse(data[RRServerMsgKeys.succeeded], out connectionSuccessful)) {
            if(connectionSuccessful) {
                message.text = "Connection Successful\nLoading Game...";
                SceneManager.LoadScene(sceneToLoad);
            } else {
                message.text = "Failed to Connect to Server";
            }
        }
        rrserver.StopListening(
            RRServerMsgType.ServerConnectionStatusChange,
            HandleServerConnectionStatusChange);
    }
}
