﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageableText : MonoBehaviour {

    [SerializeField] private Text damageText; // set in editor

    // lifetime tracking variables
    private float timer = 0;
    private const float lifeTime = 1f;

    // amount variables
    public int amount {
        get { return int.Parse(damageText.text); }
        set { damageText.text = value.ToString(); }
    }

    // height variables
    public float initialHeight { set { transform.Translate(0, value, 0); } }
    private const float deltaHeight = 1f;

    // Color variables
    public Color color {
        set {
            initialColor = value;
            finalColour = value;
            finalColour.a = 0f;
        }
    }
    private Color initialColor = Color.black;
    private Color finalColour = Color.clear;

    void Start () {
        Destroy(gameObject, lifeTime);
    }

    void Update () {
        transform.Translate(0, deltaHeight * Time.deltaTime * lifeTime, 0);
        damageText.color = Color.Lerp(
            initialColor,
            finalColour,
            timer / lifeTime
        );
        timer += Time.deltaTime;
    }
}
