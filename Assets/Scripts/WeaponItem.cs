﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItem : MonoBehaviour {

    private bool hasTriggered;
    public string weapon;

    void Awake() {
        hasTriggered = false;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(hasTriggered) {
            return;
        }

        if(other.gameObject.name == "PlayerActionBox") {
            other.gameObject.GetComponent<PlayerActionBox>()
                .GiveWeapon(Resources.Load<GameObject>("Prefabs/" + weapon));
            hasTriggered = true;
            Destroy(transform.parent.gameObject);
        }
    }
}
