﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPrize : MonoBehaviour {

    public List<GameObject> hitList;
    public GameObject jail;
    public bool initialized = false;

    // Update is called once per frame
    void Update () {
        if(hitList != null) {
            var somethingAlive = false;
            foreach(var gameObject in hitList) {
                if(gameObject != null) {
                    somethingAlive = true;
                    break;
                }
            }
            if(!somethingAlive) {
                // destroy self and the jail to grant prize
                Destroy(gameObject);
            }
        }

    }

    public void Init(List<GameObject> newHitList, GameObject prize) {
        initialized = true;
        hitList = newHitList;
        prize.transform.position = jail.transform.position;
    }

    public void InitDefenceItem(List<GameObject> newHitList) {
        var prefabs = Resources.LoadAll<GameObject>("Prefabs/Items/Defence");
        Init(newHitList, Instantiate(prefabs[Random.Range(0, prefabs.Length)]));
    }

    public void InitRandomItem(List<GameObject> newHitList) {
        var prefabs = Resources.LoadAll<GameObject>("Prefabs/Items");
        Init(newHitList, Instantiate(prefabs[Random.Range(0, prefabs.Length)]));
    }
}
