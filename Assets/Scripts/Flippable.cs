﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flippable : MonoBehaviour {
    public bool flipped { get; private set; }
    public int factor { get; private set; }
    public bool flipLocalPosition { get; set; }
    public bool flipSprite { get; set; }

    private SpriteRenderer rendererToFlip;

    void Awake() {
        flipped = false;
        factor = 1;
        flipLocalPosition = true;

        rendererToFlip = GetComponent<SpriteRenderer>();
        if(rendererToFlip != null) {
            flipSprite = true;
        } else {
            flipSprite = false;
        }
    }

    public void Flip() {
        flipped = !flipped;
        factor *= -1;
        if(flipSprite) {
            rendererToFlip.flipX = flipped;
        }

        if(flipLocalPosition) {
            transform.localPosition = new Vector3(
              transform.localPosition.x * -1,
              transform.localPosition.y,
              transform.localPosition.z);
        }
    }
}
