﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Damageable : MonoBehaviour {

    [SerializeField] protected float health;
    public float Health() { return health; }
    [SerializeField] protected int maxHealth = 1;

    protected GameObject damageableTextPrefab {
        get { return Resources.Load<GameObject>("Prefabs/DamageableText"); }
    }
    [SerializeField] protected float showDamageHeight = 0;
    [SerializeField] protected Color damageColor = Color.red;
    [SerializeField] protected Color healColor = Color.green;

    protected void ShowDamage(int n) {
        var text = Instantiate(damageableTextPrefab, transform.position, transform.rotation);
        text.GetComponent<DamageableText>().amount = n;
        text.GetComponent<DamageableText>().initialHeight = showDamageHeight;
        text.GetComponent<DamageableText>().color = damageColor;
    }

    protected void ShowHeal(int n) {
        var text = Instantiate(damageableTextPrefab, transform.position, transform.rotation);
        text.GetComponent<DamageableText>().amount = n;
        text.GetComponent<DamageableText>().initialHeight = showDamageHeight;
        text.GetComponent<DamageableText>().color = healColor;
    }

    public virtual void Heal(int n) {
        health += n;
        health = System.Math.Min(health, maxHealth);
        ShowHeal(n);
    }

    public virtual void TakeDamage(int amount, Vector3 knockback) {
        health -= amount;
        if(health <= 0) {
            Destroy(gameObject);
        }
        gameObject.GetComponent<Rigidbody2D>().velocity = knockback;
        ShowDamage(amount);
    }

    public virtual void TakeFireDamage(int amount, Vector3 knockback) {
        TakeDamage(amount, knockback);
    }
}
